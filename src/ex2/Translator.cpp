#include "Translator.h"
#include <qtextstream.h>
#include <QFile>
#include <QDebug>

void Translator::useCorpus(Corpus *corpus)
{
    mCorpus = corpus;
    
    connect(mCorpus, &Corpus::corpusChanged, this, &Translator::corpusChanged);
}

void Translator::translate()
{
    mTranslation.clear();
    
    auto sentences = mSource.split("\n");
    for (auto sentence : sentences)
    {
        auto words = sentence.split(" ");
        
        for(auto word : words)
        {
            QString translation = "";
            
            translation = mCorpus->getBestTranslation(word);
            qDebug() << "Translating word " << word << " with " << translation;
            
            if (translation.isEmpty())
            {
                translation = word;
            }
            
            mTranslation.append(translation + " ");
        }
        
        mTranslation.append("\n");
    }
    
    emit translationChanged(mTranslation);
}

void Translator::loadText(QString url)
{
    QFile file(url);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        mSource = stream.readAll();
        file.close();
        
        emit textLoaded(mSource);
    }
}

void Translator::saveText(QString url)
{
    QFile file(url);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << mTranslation;
        file.close();
    }
}

void Translator::sourceChanged(QString source)
{
    mSource = source;
    
    translate();
}

void Translator::corpusChanged()
{
    // update translation
    translate();
}
