import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.0

// The main application window
ApplicationWindow {
    id: mainWindow
    title: qsTr("Übung 2")
    visible: true
    minimumWidth: 200
    minimumHeight: 200
    width: 1200
    height: 600
    
    FileDialog {
        id: openSourceCorpusDialog
        title: "Open Source Corpus"
        nameFilters: [ "EuroParl" ]
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        modality: Qt.ApplicationModal
        onAccepted: {
            corpora.loadSourceCorpus(openSourceCorpusDialog.fileUrl)
        }
    }
    
    FileDialog {
        id: openTranslationCorpusDialog
        title: "Open Translation Corpus"
        nameFilters: [ "EuroParl" ]
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        modality: Qt.ApplicationModal
        onAccepted: {
            corpora.loadTranslationCorpus(openTranslationCorpusDialog.fileUrl)
        }
    }
    
    FileDialog {
        id: openTextDialog
        title: "Open Text"
        nameFilters: [ "Plain Text (*.txt)" ]
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        modality: Qt.ApplicationModal
        onAccepted: {
            translator.loadText(openTextDialog.fileUrl)
        }
    }
    
    FileDialog {
        id: saveTextDialog
        title: "Save Text"
        nameFilters: [ "Plain Text (*.txt)" ]
        selectExisting: false
        selectFolder: false
        selectMultiple: false
        modality: Qt.ApplicationModal
        onAccepted: {
            translator.saveText(saveTextDialog.fileUrl)
        }
    }
    
    GridLayout {
        id: grid
        columns: 3
        anchors.fill: parent
        
        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            
            Button {
                id: openSourceCorpus
                Layout.fillWidth: true
                
                text: "Open Source Corpus"
                
                onClicked: openSourceCorpusDialog.open()
            }
            
            Button {
                id: openTranslationCorpus
                Layout.fillWidth: true
                
                text: "Open Translation Corpus"
                
                onClicked: openTranslationCorpusDialog.open()
            }
            
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Corpus"
                
                TableView {
                    id: corpusTable
                    anchors.fill: parent
                    
                    TableViewColumn {
                        id: corpusSource
                        title: "Source"
                        role: "source"
                        movable: false
                    }
                    
                    TableViewColumn {
                        id: corpusTranslated
                        title: "Translated"
                        role: "translated"
                        movable: false
                    }
                    
                    model: ListModel {
                        id: corpusTableList
                    }
                }
            }
            
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Probabilities"
                
                TableView {
                    id: probabilitiesTable
                    anchors.fill: parent
                    
                    TableViewColumn {
                        id: probabilitiesSource
                        title: "Source"
                        role: "source"
                        movable: false
                    }
                    
                    TableViewColumn {
                        id: probabilitiesTranslated
                        title: "Translated"
                        role: "translated"
                        movable: false
                    }
                    
                    TableViewColumn {
                        id: probabilitiesProbability
                        title: "Probability"
                        role: "probability"
                        movable: false
                    }
                    
                    model: ListModel {
                        id: probabilitiesTableList
                    }
                }
            }
        }
        
        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            
            Button {
                id: openText
                Layout.fillWidth: true
                
                text: "Open Text"
                
                onClicked: openTextDialog.open()
            }
            
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Original Text"
                
                TextArea {
                    id: originalText
                    anchors.fill: parent
                    text: ""
                    
                    onTextChanged: translator.sourceChanged(originalText.text)
                }
            }
        }
        
        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            
            Button {
                id: saveText
                Layout.fillWidth: true
                
                text: "Save Text"
                
                onClicked: saveTextDialog.open()
            }
            
            GroupBox {
                Layout.fillHeight: true
                Layout.fillWidth: true
                title: "Translated Text"
                
                TextArea {
                    id: translatedText
                    anchors.fill: parent
                    text: ""
                }
            }
        }
    }
    
    Connections {
        target: translator
        
        onTranslationChanged: {
            translatedText.text = translation;
        }
        
        onTextLoaded: {
            originalText.text = text;
        }
    }
    
    Connections {
        target: corpora
        
        onSentenceAdded: {
            corpusTableList.append({
                source: source,
                translated: translation
            });
        }
        
        onProbabilityCalculated: {
            probabilitiesTableList.append({
                source: source,
                translated: translation,
                probability: probability.toFixed(5)
            });
        }
    }
}

