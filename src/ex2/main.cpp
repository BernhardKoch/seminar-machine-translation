#include <QTextStream>
#include <QQmlContext>
#include <QApplication>
#include <QQmlApplicationEngine>

#include "Corpus.h"
#include "Translator.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    
    QQmlApplicationEngine engine;
    
    Corpus corpus;
    Translator translator;
    
    translator.useCorpus(&corpus);
    
    engine.rootContext()->setContextProperty("translator", &translator);
    engine.rootContext()->setContextProperty("corpora", &corpus);
    
    engine.addImportPath("qrc:/");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    
    return app.exec();
}

