#include "Corpus.h"

#include <QFile>
#include <QDebug>

QString Corpus::getBestTranslation(QString word)
{
    TranslationProbability *bestTranslation = new TranslationProbability;
    
    word = word.trimmed().toLower();

    auto possibilities = mProbabilities.find(word);
    if (possibilities != mProbabilities.end())
    {
        for (auto possibility : possibilities->second)
        {
            qDebug() << "Searching for best Translation... " << possibility->word << " = " << possibility->probability << " > " << bestTranslation->probability << " - " << bestTranslation->word;
            bool sameWordType = isStopWord(word) == isStopWord(possibility->word);
            if (possibility->probability > bestTranslation->probability && sameWordType)
            {
                bestTranslation = possibility;
            }
        }
    }
    else
    {
        qDebug() << "Best Probability: Could not find word " << word;
    }
    
    qDebug() << "Best Probability: " << word << " - " << bestTranslation->word << " => " << bestTranslation->probability;
    
    return bestTranslation->word;
}

void Corpus::loadSourceCorpus(QUrl url)
{
    mSourceCorpus = url;
}

void Corpus::loadTranslationCorpus(QUrl url)
{
    mTranslationCorpus = url;
    
    loadCorpus();
}

void Corpus::loadCorpus()
{
    qDebug() << "Source Corpus: " << mSourceCorpus.path();
    qDebug() << "Translation Corpus: " << mTranslationCorpus.path();
    
    QFile sourceFile(mSourceCorpus.path());
    QFile translationFile(mTranslationCorpus.path());
    
    if (!sourceFile.open(QIODevice::ReadOnly)
        || !translationFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to open Corpus files: " << sourceFile.errorString() << " ;; " << translationFile.errorString();
    }
    else
    {
        QTextStream sourceStream(&sourceFile);
        QTextStream translationStream(&translationFile);
        
        unsigned int maxLinesToProcess = 100;
        unsigned int currentLine = 0;
        
        while (!sourceStream.atEnd() && !translationStream.atEnd() && currentLine < maxLinesToProcess)
        {
            QString sourceLine = sourceStream.readLine();
            QString translationLine = translationStream.readLine();
            
            auto sourceWords = sourceLine.split(" ");
            auto translationWords = translationLine.split(" ");
            
            for (auto sourceWord : sourceWords)
            {
                for (auto translationWord : translationWords)
                {
                    addOccurence(sourceWord.trimmed().toLower(), translationWord.trimmed().toLower(), translationWords.length());
                }
            }
            
            mSentences.insert(std::pair<QString, QString> (sourceLine, translationLine));
            emit sentenceAdded(sourceLine, translationLine);
            
            currentLine++;
        }
        
        sourceFile.close();
        translationFile.close();
        
        qDebug() << "Finished Loading Corpus";
        emit corpusChanged();
        
        calculateAverageOccurences();
        
        // Emit all probabilities
        for (auto translations : mProbabilities)
        {
            for (auto transProb : translations.second)
            {
                emit probabilityCalculated(translations.first, transProb->word, transProb->probability);
            }
        }
    }
}

void Corpus::addOccurence(QString word, QString translation, unsigned int sentenceLength)
{
    double probability = 1.0 / sentenceLength;
    double previousProbability = 0;
    double newProbability = 0;
    
    qDebug() << "ADD OCCURENCE new Probability: " << probability;
    
    auto possibilities = mProbabilities.find(word);
    if (possibilities != mProbabilities.end())
    {
        bool found = false;
        for (auto possibility : possibilities->second)
        {
            if (possibility->word == translation)
            {
                previousProbability = possibility->probability;
                possibility->addProbability(probability);
                newProbability = possibility->probability;
                qDebug() << "Updated Probability...";
                found = true;
                break;
            }
        }
        
        if (!found)
        {
            TranslationProbability *transProb = new TranslationProbability;
            transProb->word = translation;
            previousProbability = transProb->probability;
            transProb->addProbability(probability);
            newProbability = transProb->probability;
            possibilities->second.push_back(transProb);
            qDebug() << "Added Translation...";
        }
    }
    else
    {
        std::vector<TranslationProbability*> possibleTranslations;
        TranslationProbability *transProb = new TranslationProbability;
        transProb->word = translation;
        previousProbability = transProb->probability;
        transProb->addProbability(probability);
        newProbability = transProb->probability;
        possibleTranslations.push_back(transProb);
        mProbabilities.insert(std::pair<QString, std::vector<TranslationProbability*> >(word, possibleTranslations));
        qDebug() << "Added Unknown Word...";
    }
    
    addWordOccurence(word);
    addWordOccurence(translation);
}

void Corpus::addWordOccurence(QString word)
{
    auto possibilities = mOccurences.find(word);
    if (possibilities != mOccurences.end())
    {
        possibilities->second++;
    }
    else
    {
        mOccurences.insert(std::pair<QString, unsigned int> (word, 1));
    }
}

void Corpus::calculateAverageOccurences()
{
    for(auto occurence : mOccurences)
    {
        mAverageOccurence += occurence.second;
    }
    
    mAverageOccurence /= mOccurences.size();
}

bool Corpus::isStopWord(QString word)
{
    word = word.trimmed().toLower();
    
    float threshold = mAverageOccurence;
    
    qDebug() << "Is Stop Word: " << word << " Threshold: " << threshold << " Average: " << mAverageOccurence;
    
    auto occurences = mOccurences.find(word);
    if (occurences != mOccurences.end() && occurences->second >= threshold)
        return true;
    else
        return false;
}
