#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include "Corpus.h"

#include <QObject>

class Translator: public QObject
{
    Q_OBJECT
public:
    void useCorpus(Corpus *corpus);
    void translate();
    void corpusChanged();
    
signals:
    void translationChanged(QString translation);
    void textLoaded(QString text);
    
public slots:
    void loadText(QString url);
    void saveText(QString url);
    void sourceChanged(QString source);
    
private:
    Corpus *mCorpus;
    QString mSource;
    QString mTranslation;
};

#endif // TRANSLATOR_H
