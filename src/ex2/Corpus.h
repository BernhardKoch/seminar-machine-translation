#ifndef CORPUS_H
#define CORPUS_H

#include <QObject>
#include <QVariantList>
#include <QUrl>
#include <map>
#include <vector>

class Corpus: public QObject
{
    Q_OBJECT
    
public:
    struct TranslationProbability
    {
        QString word;
        double probability;
        
        TranslationProbability()
        {
            word = "";
            probability = 0;
        }
        
        TranslationProbability (QString w)
        {
            word = w;
            probability = 0;
        }
        
        void addProbability(double prob)
        {
            probability += prob;
        }
    };
    
public:
    QString getBestTranslation(QString word);
    
signals:
    void corpusChanged();
    void sentenceAdded(QString source, QString translation);
    void probabilityCalculated(QString source, QString translation, double probability);
    
public slots:
    void loadSourceCorpus(QUrl url);
    void loadTranslationCorpus(QUrl url);
    
private:
    void loadCorpus();
    void addOccurence(QString word, QString translation, unsigned int sentenceLength);
    void addWordOccurence(QString word);
    void calculateAverageOccurences();
    bool isStopWord(QString word);
    
    QUrl mSourceCorpus;
    QUrl mTranslationCorpus;
    
    std::map<QString, std::vector<TranslationProbability*> > mProbabilities;
    std::map<QString, QString> mSentences;
    std::map<QString, unsigned int> mOccurences;
    double mAverageOccurence;
};

#endif // CORPUS_H
