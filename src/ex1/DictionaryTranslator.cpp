#include "DictionaryTranslator.h"

void DictionaryTranslator::setSource(std::string source)
{
    mSource = QString::fromStdString(source);
    
    translate();
}

void DictionaryTranslator::addToDictionary(std::string original, std::string translation)
{
    mDictionary.insert(std::pair<QString, QString> (QString::fromStdString(original), QString::fromStdString(translation)));
    
    emit termAdded(QString::fromStdString(original), QString::fromStdString(translation));
}

void DictionaryTranslator::translate()
{
    mTranslation.clear();

    auto sentences = mSource.split("\n");
    for (auto sentence : sentences)
    {
        auto words = sentence.split(" ");
        
        for(auto word : words)
        {
            QString translation = "";
            
            word = word.trimmed();
            
            auto translated = mDictionary.find(word);
            
            if (translated != mDictionary.end())
            {
                translation = translated->second;
            }
            else
            {
                translation = word;
            }
            
            mTranslation.append(translation + " ");
        }
        
        mTranslation.append("\n");
    }
    
    emit translationChanged(mTranslation);
}

void DictionaryTranslator::sourceChanged(QString source)
{
    mSource = source;
    translate();
}

void DictionaryTranslator::addDictionaryTerm(QString original, QString translation)
{
    mDictionary.insert(std::pair<QString, QString> (original, translation));
    
    emit termAdded(original, translation);
    
    translate();
}
