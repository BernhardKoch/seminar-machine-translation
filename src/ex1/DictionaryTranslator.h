#ifndef DICTIONARYTRANSLATOR_H
#define DICTIONARYTRANSLATOR_H

#include <map>
#include <string>
#include <QObject>

class DictionaryTranslator: public QObject
{
    Q_OBJECT
public:
    void setSource(std::string source);
    void addToDictionary(std::string original, std::string translation);
    void translate();
    
signals:
    void termAdded(QString original, QString translation);
    void translationChanged(QString translation);
    
public slots:
    void sourceChanged(QString source);
    void addDictionaryTerm(QString original, QString translation);
    
private:
    QString mSource;
    QString mTranslation;
    std::map<QString, QString> mDictionary;
};

#endif // DICTIONARYTRANSLATOR_H
