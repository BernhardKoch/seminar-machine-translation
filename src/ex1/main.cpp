#include <QTextStream>
#include <QQmlContext>
#include <QApplication>
#include <QQmlApplicationEngine>

#include "DictionaryTranslator.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    
    QQmlApplicationEngine engine;
    
    DictionaryTranslator translator;
    
    engine.rootContext()->setContextProperty("translator", &translator);
    
    engine.addImportPath("qrc:/");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    
    return app.exec();
}

