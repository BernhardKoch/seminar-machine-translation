import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.0

// The main application window
ApplicationWindow {
    id: mainWindow
    title: qsTr("Übung 1")
    visible: true
    minimumWidth: 200
    minimumHeight: 200
    width: 1200
    height: 600
    
    GridLayout {
        id: grid
        columns: 3
        anchors.fill: parent
        
        GroupBox {
            Layout.fillHeight: true
            Layout.fillWidth: grid.width / 3
            title: "Text"
            
            TextArea {
                id: originalText
                anchors.fill: parent
                text: ""
                
                onTextChanged: translator.sourceChanged(originalText.text)
                
            }
        }
        
        GroupBox {
            Layout.fillHeight: true
            Layout.fillWidth: grid.width / 3
            title: "Translation"
            
            TextArea {
                id: translatedText
                anchors.fill: parent
                text: ""
                
            }
        }
        
        GroupBox {
            Layout.fillHeight: true
            Layout.fillWidth: grid.width / 3
            title: "Dictionary"
            
            ColumnLayout {
                anchors.fill: parent
                
                TableView {
                    id: dictionaryTable
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    
                    TableViewColumn {
                        id: dictionarySource
                        title: "Original Word"
                        role: "original"
                        movable: false
                    }
                    
                    TableViewColumn {
                        id: dictionaryTranslated
                        title: "Translated Word"
                        role: "translated"
                        movable: false
                    }
                    
                    model: ListModel {
                        id: dictionaryTableList
                    }
                }
                
                GridLayout {
                    columns: 3
                    Layout.fillWidth: true
                    
                    TextField {
                        id: addDictionaryOriginal
                    }
                    
                    TextField {
                        id: addDictionaryTranslated
                    }
                    
                    Button {
                        text: "Add Term"
                        
                        onClicked: {
                            translator.addDictionaryTerm(addDictionaryOriginal.text, addDictionaryTranslated.text);
                            addDictionaryOriginal.text = "";
                            addDictionaryTranslated.text = "";
                            addDictionaryOriginal.forceActiveFocus();
                        }
                        
                        Keys.onEnterPressed: {
                            translator.addDictionaryTerm(addDictionaryOriginal.text, addDictionaryTranslated.text);
                            addDictionaryOriginal.text = "";
                            addDictionaryTranslated.text = "";
                            addDictionaryOriginal.forceActiveFocus();
                        }
                    }
                }
            }
        }
    }
    
    Connections {
        target: translator
        
        onTranslationChanged: {
            translatedText.text = translation;
        }
        
        onTermAdded: {
            dictionaryTableList.append({
                original: original,
                translated: translation
            });
        }
    }
}

